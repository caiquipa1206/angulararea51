const jwt = require("jwt-simple")
const moment = require("moment")
const randToken = require("rand-token")
const tokenCrtl = {};
const palabraSecreta = "F529ED6D-8ADC-49D1-89E8-5CD171326B56"

tokenCrtl.createAccessToken = (_id, nombre, rol) => {
	const payload = {
		_id,
		nombre,
		rol,
		iat: moment().unix(),
		exp: moment().add(60, "seconds").unix()
	}

	const accessToken = jwt.encode(payload, palabraSecreta)

	return accessToken
}

tokenCrtl.createRefreshToken = () => {
	const refreshToken = randToken.uid(256)

	return refreshToken
}

tokenCrtl.decodeAccessToken = (accessToken) => {
	const promesa = new Promise((resolve, reject) => {
		try {
			const payload = jwt.decode(accessToken, palabraSecreta)
			console.log(payload)
			resolve(payload)
		} catch (error) {
			if (error.message.toLowerCase() == "token expired") {
				resolve({
					status: 401,
					message: 'token has expired'
				})
			} else {
				resolve({
					status: 500,
					message: 'Token invalid"'
				})
			}
		}
	})

	return promesa
}
module.exports = tokenCrtl;