const Persona = require('../models/persona');

const personaCtrl = {};

personaCtrl.getPersonas = async (req, res, next) => {

	const personas = await Persona.find();
	res.json(personas);
};

personaCtrl.createPersona = async (req, res, next) => {

	const persona = new Persona({
		idTipoPersona: req.body.idTipoPersona,
		idTipoDocumento: req.body.idTipoDocumento,
		nroDocumento: req.body.nroDocumento,
		nombres: req.body.nombres,
		apellidoPaterno: req.body.apellidoPaterno,
		apellidoMaterno: req.body.apellidoMaterno,
		celular: req.body.celular,
		email: req.body.email,
		estadoCivil: req.body.estadoCivil
	});
	await persona.save();
	res.json({ status: 'Persona created' });
};

personaCtrl.getPersona = async (req, res, next) => {
	const { id } = req.params;
	const persona = await Persona.findById(id);
	res.json(persona);
};

personaCtrl.editPersona = async (req, res, next) => {
	const { id } = req.params;
	const persona = {
		idTipoPersona: req.body.idTipoPersona,
		idTipoDocumento: req.body.idTipoDocumento,
		nroDocumento: req.body.nroDocumento,
		nombres: req.body.nombres,
		apellidoPaterno: req.body.apellidoPaterno,
		apellidoMaterno: req.body.apellidoMaterno,
		celular: req.body.celular,
		email: req.body.email,
		estadoCivil: req.estadoCivil
	};
	await Persona.findByIdAndUpdate(id, { $set: persona }, { new: true });
	res.json({ status: 'Persona Updated' });
};

personaCtrl.deletePersona = async (req, res, next) => {
	await Persona.findByIdAndRemove(req.params.id);
	res.json({ status: 'Persona Deleted' });
};

module.exports = personaCtrl;