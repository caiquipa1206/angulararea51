const Usuario = require('../models/usuario');
const token = require('../services/token.services');
const usuarioCrtl = {};

usuarioCrtl.getUsuarios = async (req, res, next) => {
	const usuarios = await Usuario.find();
	res.json(usuarios);
};
usuarioCrtl.createUsuario = async (req, res, next) => {
	const usuario = new Usuario({
		identidad: req.body.identidad,
		usuario: req.body.usuario,
		password: req.body.password,
		idperfil: req.body.idperfil,
		refreshToken: req.body.refreshToken,
		nombre: req.body.nombre,
		apellido: req.body.apellido
	});
	await usuario.save();
	res.json({ status: 'Usuario created' });
};

usuarioCrtl.getUsuario = async (req, res, next) => {
	const { id } = req.params;
	const usuario = await Usuario.findById(id);
	res.json(usuario);
};
usuarioCrtl.deleteUsuario = async (req, res, next) => {
	await Usuario.findByIdAndRemove(req.params.id);
	res.json({ status: 'Usuario Deleted' });
};
usuarioCrtl.editUsuario = async (req, res, next) => {
	const { id } = req.params;
	const usuario = {
		identidad: req.body.identidad,
		usuario: req.body.usuario,
		password: req.body.password,
		idperfil: req.body.idperfil,
		refreshToken: req.body.refreshToken
	};
	await Usuario.findByIdAndUpdate(id, { $set: usuario }, { new: true });
	res.json({ status: 'Usuario Updated' });
};

usuarioCrtl.login = async (req, res, next) => {
	//console.log(req.body);
	const usuario = req.body.usuario.toLowerCase().trim()
	const password = req.body.password.trim()

	const usuarioGet = await Usuario.findOne({ usuario, password })

	if (usuarioGet) {
		//console.log(usuarioGet);
		const accessToken = token.createAccessToken(usuarioGet._id, usuarioGet.usuario, usuarioGet.idperfil)
		const refreshToken = token.createRefreshToken();
		//console.log(refreshToken)
		//console.log(usuarioGet)
		const usuario = {
			identidad: usuarioGet.identidad,
			usuario: usuarioGet.usuario,
			password: usuarioGet.password,
			idperfil: usuarioGet.idperfil,
			refreshToken: refreshToken
		};
		await Usuario.findByIdAndUpdate(usuarioGet._id, { $set: usuario }, { new: true });
		return res.json({ accessToken, refreshToken, status: 200, message: 'Ok' })
	}

	return res
		.json({
			status: 404,
			message: "User not found",
			accessToken: '',
			refreshToken: ''
		})
}

usuarioCrtl.newAccessToken = async (req, res, next) => {
	const refreshToken = req.body.refreshToken

	const usuario = await Usuario.findOne({ refreshToken })

	if (usuario) {
		const accessToken = token.createAccessToken(usuario._id, usuario.nombre, usuario.idperfil)

		res.json({ accessToken })
	} else {
		res
			.status(404)
			.json({
				status: 404,
				message: "User not logged"
			})
	}
}

module.exports = usuarioCrtl;