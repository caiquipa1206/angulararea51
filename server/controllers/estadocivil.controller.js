const EstadiCivil = require('../models/estadocivil');
const tokens = require('../services/token.services')
const estadoCivilCtrl = {};

estadoCivilCtrl.getEstadosCiviles = async (req, res, next) => {

	if (req.headers && req.headers["authorization"]) {
		const accessToken = req.headers["authorization"]
		console.log(accessToken)
		let responseToken = false;
		const estadosCiviles = await EstadiCivil.find();
		tokens.decodeAccessToken(accessToken).then(
			payload => {
				res.locals._id = payload._id
				res.locals.nombre = payload.nombre
				res.locals.apellido = payload.apellido
			},
			res.json(estadosCiviles)
		).catch(
			res.json({
				status: 500,
				message: "error traducir access token"
			})
		)
	}
};

estadoCivilCtrl.createEstadoCivil = async (req, res, next) => {

	const estadoCivil = new EstadiCivil({
		identidad: req.body.identidad,
		estadoCivil: req.body.estadoCivil
	});
	await estadoCivil.save();
	res.json({ status: 'Estado created' });
};
estadoCivilCtrl.getEstadoCivil = async (req, res, next) => {
	const { id } = req.params;
	const estadocivil = await EstadiCivil.findById(id);
	res.json(estadocivil);
};
estadoCivilCtrl.deleteEstadoCivil = async (req, res, next) => {
	await EstadiCivil.findByIdAndRemove(req.params.id);
	res.json({ status: 'Tipo persona Deleted' });
};
estadoCivilCtrl.editEstadoCivil = async (req, res, next) => {
	const { id } = req.params;
	const estadocivil = {
		identidad: req.body.identidad,
		estadoCivil: req.body.estadoCivil
	};
	await EstadiCivil.findByIdAndUpdate(id, { $set: estadocivil }, { new: true });
	res.json({ status: 'Tipo persona Updated' });
};
module.exports = estadoCivilCtrl;