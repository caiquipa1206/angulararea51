const TipoDocumento = require('../models/tipodocumento');

const tipodocumentoCtrl = {};

tipodocumentoCtrl.getTipodocumentos = async (req, res, next) => {
	const tipodocumentos = await TipoDocumento.find();
	res.json(tipodocumentos);
};

tipodocumentoCtrl.createTipodocumento = async (req, res, next) => {
	const tipodocumento = new TipoDocumento({
		identidad: req.body.identidad,
		tipoDocumento: req.body.tipoDocumento
	});
	await tipodocumento.save();
	res.json({ status: 'Tipopersona created' });
};
tipodocumentoCtrl.getTipodocumento = async (req, res, next) => {
	const { id } = req.params;
	const tipodocumento = await TipoDocumento.findById(id);
	res.json(tipodocumento);
};
tipodocumentoCtrl.deleteTipoDocumento = async (req, res, next) => {
	await TipoDocumento.findByIdAndRemove(req.params.id);
	res.json({ status: 'Tipo persona Deleted' });
};
tipodocumentoCtrl.editTipoDocumento = async (req, res, next) => {
	const { id } = req.params;
	const tipodocumento = {
		identidad: req.body.identidad,
		tipoDocumento: req.body.tipoDocumento
	};
	await TipoDocumento.findByIdAndUpdate(id, { $set: tipodocumento }, { new: true });
	res.json({ status: 'Tipo persona Updated' });
};
module.exports = tipodocumentoCtrl;