const Tipopersona = require('../models/tipopersona');

const tipopersonaCtrl = {};

tipopersonaCtrl.getTipopersonas = async (req, res, next) => {
	const tipopersonas = await Tipopersona.find();
	res.json(tipopersonas);
};
tipopersonaCtrl.createTipopersona = async (req, res, next) => {
	const tipopersona = new Tipopersona({
		identidad: req.body.identidad,
		tipo: req.body.tipo
	});
	await tipopersona.save();
	res.json({ status: 'Tipopersona created' });
};

tipopersonaCtrl.getTipopersona = async (req, res, next) => {
	const { id } = req.params;
	const tipopersona = await Tipopersona.findById(id);
	res.json(tipopersona);
};
tipopersonaCtrl.deleteTipopersona = async (req, res, next) => {
	await Tipopersona.findByIdAndRemove(req.params.id);
	res.json({ status: 'Tipo persona Deleted' });
};
tipopersonaCtrl.editTipoPersona = async (req, res, next) => {
	const { id } = req.params;
	const tipopersona = {
		identidad: req.body.identidad,
		tipo: req.body.tipo
	};
	await Tipopersona.findByIdAndUpdate(id, { $set: tipopersona }, { new: true });
	res.json({ status: 'Tipo persona Updated' });
};
module.exports = tipopersonaCtrl;