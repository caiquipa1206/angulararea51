const express = require('express');
const cors = require('cors');
const app = express();
const routetipodocumento = "./routes/tipodocumento.routes"
const { mongoose } = require('./database');

// Settings
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(cors({ origin: 'http://localhost:4200' }));
app.use(express.json());

// Routes
app.use('/api/login', require('./routes/login.routes'));
app.use('/api/employees', require('./routes/employee.routes'));
app.use('/api/tipopersonas', require('./routes/tipopersona.routes'));
app.use('/api/tipodocumentos', require('./routes/tipodocumento.routes'));
app.use('/api/estadocivil', require('./routes/estadocivil.routes'));
app.use('/api/personas', require('./routes/persona.routes'));
app.use('/api/usuarios', require('./routes/usuario.routes'));

//app.use("/tipodocumento", routetipodocumento)
// starting the server
app.listen(app.get('port'), () => {
	console.log(`server on port ${app.get('port')}`);
});