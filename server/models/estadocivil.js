const mongoose = require('mongoose');
const { Schema } = mongoose;

const estadocivilSchema = new Schema({
	identidad: { type: Number, required: true },
	estadoCivil: { type: String, required: true }
});

module.exports = mongoose.model('estadocivil', estadocivilSchema);