const mongoose = require('mongoose');
const { Schema } = mongoose;

const personaSchema = new Schema({
	idTipoPersona: { type: String, required: true },
	idTipoDocumento: { type: String, required: true },
	nroDocumento: { type: String, required: true },
	nombres: { type: String, required: true },
	apellidoPaterno: { type: String, required: true },
	apellidoMaterno: { type: String, required: true },
	celular: { type: String, required: true },
	email: { type: String, required: true },
	estadoCivil: { type: String, required: true }
});

module.exports = mongoose.model('Persona', personaSchema);