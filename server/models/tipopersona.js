const mongoose = require('mongoose');
const { Schema } = mongoose;

const tipopersonaSchema = new Schema({
	identidad: { type: Number, required: true },
	tipo: { type: String, required: true }
});

module.exports = mongoose.model('tipopersona', tipopersonaSchema);