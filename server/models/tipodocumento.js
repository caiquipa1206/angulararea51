const mongoose = require('mongoose');
const { Schema } = mongoose;

const tipodocumentoSchema = new Schema({
	identidad: { type: Number, required: true },
	tipoDocumento: { type: String, required: true }
});

module.exports = mongoose.model('tipodocumento', tipodocumentoSchema);