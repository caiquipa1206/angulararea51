const mongoose = require('mongoose');
const { Schema } = mongoose;

const usuarioSchema = new Schema({
	identidad: { type: Number, required: true },
	usuario: { type: String, required: true },
	password: { type: String, required: true },
	idperfil: { type: Number, required: true },
	refreshToken: { type: String, required: true },
	nombre: { type: String, required: true },
	apellido: { type: String, required: true },
});

module.exports = mongoose.model('usuario', usuarioSchema);