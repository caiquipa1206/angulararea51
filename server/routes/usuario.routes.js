const express = require('express');
const router = express.Router();

const usuario = require('../controllers/usuario.controller');

router.get('/', usuario.getUsuarios);
router.get('/:id', usuario.getUsuario);
router.post('/', usuario.createUsuario);
router.put('/:id', usuario.editUsuario);
router.delete('/:id', usuario.deleteUsuario);
router.post('/', usuario.login);
module.exports = router;