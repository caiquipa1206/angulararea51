const express = require('express');
const router = express.Router();

const tipopersona = require('../controllers/tipopersona.controller');

router.get('/', tipopersona.getTipopersonas);
router.get('/:id', tipopersona.getTipopersona);
router.post('/', tipopersona.createTipopersona);
router.put('/:id', tipopersona.editTipoPersona);
router.delete('/:id', tipopersona.deleteTipopersona);
module.exports = router;