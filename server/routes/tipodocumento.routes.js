const express = require('express');
const router = express.Router();

const tipodocumento = require('../controllers/tipodocumento.controller');

router.get('/', tipodocumento.getTipodocumentos);
router.get('/:id', tipodocumento.getTipodocumento);
router.post('/', tipodocumento.createTipodocumento);
router.put('/:id', tipodocumento.editTipoDocumento);
router.delete('/:id', tipodocumento.deleteTipoDocumento);
module.exports = router;