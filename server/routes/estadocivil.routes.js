const express = require('express');
const router = express.Router();

const estadocivil = require('../controllers/estadocivil.controller');

router.get('/', estadocivil.getEstadosCiviles);
router.get('/:id', estadocivil.getEstadoCivil);
router.post('/', estadocivil.createEstadoCivil);

router.put('/:id', estadocivil.editEstadoCivil);
router.delete('/:id', estadocivil.deleteEstadoCivil);
module.exports = router;