export class ResponseLogin {

	// constructor(_id = '', identidad = 0, usuario = '', password = '', idperfil = 0, refreshToken = '') {
	// 	this._id = _id;
	// 	this.identidad = identidad;
	// 	this.usuario = usuario;
	// 	this.password = password;
	// 	this.idperfil = idperfil;
	// 	this.refreshToken = refreshToken;
	// }

	// _id: string;
	// identidad: number;
	// usuario: string;
	// password: string;
	// idperfil: number;
	// refreshToken: string;

	constructor(accessToken = '', refreshToken = '', status = 0, message = '') {

		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.status = status
		this.message = message
	}

	accessToken: string;
	refreshToken: string;
	status: number;
	message: string

}