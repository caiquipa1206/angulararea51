export class Persona {

	constructor(_id = '', idTipoPersona = '', idTipoDocumento = '', nroDocumento = '', nombres = '', apellidoPaterno = '', apellidoMaterno = '', celular = '', email = '', estadoCivil = '') {
		this._id = _id;
		this.idTipoPersona = idTipoPersona;
		this.idTipoDocumento = idTipoDocumento;
		this.nroDocumento = nroDocumento;
		this.nombres = nombres;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.celular = celular;
		this.email = email;
		this.estadoCivil = estadoCivil;
	}

	_id: string;
	idTipoPersona: string;
	idTipoDocumento: string;
	nroDocumento: string;
	nombres: string;
	apellidoPaterno: string;
	apellidoMaterno: string;
	celular: string;
	email: string;
	estadoCivil: string;
}