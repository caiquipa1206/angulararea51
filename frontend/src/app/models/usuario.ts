export class Usuario {

	constructor(_id = '', identidad = 0, usuario = '', password = '', idperfil = 0, refreshToken = '', nombre = '', apellido = '') {
		this._id = _id;
		this.identidad = identidad;
		this.usuario = usuario;
		this.password = password;
		this.idperfil = idperfil;
		this.refreshToken = refreshToken;
		this.nombre = nombre;
		this.apellido = apellido
	}

	_id: string;
	identidad: number;
	usuario: string;
	password: string;
	idperfil: number;
	refreshToken: string;
	nombre: string;
	apellido: string
	// nombre: { type: String, required: true },
	// apellido: { type: String, required: true },
	// 	constructor(usuario = '', password = '') {

	// 		this.usuario = usuario;
	// 		this.password = password;

	// 	}

	// 	usuario: string;
	// 	password: string;
}