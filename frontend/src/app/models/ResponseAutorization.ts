export class ResponseAutorization {
	constructor(status = 0, message = '') {
		this.status = status
		this.message = message
	}
	status: number;
	message: string
}