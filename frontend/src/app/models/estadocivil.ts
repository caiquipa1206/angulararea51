export class EstadoCivil {

	constructor(_id = '', identidad = 0, estadoCivil = '') {
		this._id = _id;
		this.identidad = identidad;
		this.estadoCivil = estadoCivil;
	}

	_id: string;
	identidad: number;
	estadoCivil: string;
}