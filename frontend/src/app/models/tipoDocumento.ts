export class TipoDocumento {

	constructor(_id = '', identidad = 0, tipoDocumento = '') {
		this._id = _id;
		this.identidad = identidad;
		this.tipoDocumento = tipoDocumento;
	}

	_id: string;
	identidad: number;
	tipoDocumento: string;
}