export class TipoPersona {

	constructor(_id = '', identidad = 0, tipo = '') {
		this._id = _id;
		this.identidad = identidad;
		this.tipo = tipo;
	}

	_id: string;
	identidad: number;
	tipo: string;
}