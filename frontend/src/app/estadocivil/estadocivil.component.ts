import { Component, OnInit } from '@angular/core';
import { EstadoCivilService } from '../services/estadocivil.service';
import { EstadoCivil } from '../models/estadocivil';
import { NgForm } from '@angular/forms';
import { ResponseAutorization } from '../models/ResponseAutorization';

@Component({
	selector: 'app-estadocivil',
	templateUrl: './estadocivil.component.html',
	styleUrls: ['./estadocivil.component.css']
})
export class EstadocivilComponent implements OnInit {

	constructor(private estadoCivilService: EstadoCivilService) {
		this.getEstadoCivilList();
	}
	listaEstadosCiviles: any;
	idTipoPersona: string;
	ngOnInit() {
	}
	getEstadoCivilList() {
		this.estadoCivilService.getEstadosCiviles().subscribe(data => {
			let valdata = (data as ResponseAutorization)
			console.log(valdata)
			if (valdata && valdata.status) {
				//if (valdata.status = 0) {

				//}
				//	else {
				alert(valdata.message)
				//	}
			}
			else {
				this.listaEstadosCiviles = data;
			}
		});
	}
	model = this.estadoCivilService.selectedEstado;
	newtipopersona() {
		this.model = new EstadoCivil('', 0, '');
	}
	editestadoCivil(persona: EstadoCivil) {
		this.estadoCivilService.getEstadoCivil(persona._id).subscribe(data => {
			console.log(data);
			this.estadoCivilService.selectedEstado = data as EstadoCivil;
			// console.log(this.tipoPersonaService.selectedTipoPersona );
			this.model = data as EstadoCivil;
			//console.log(this.model);
		});

	}
	addTipopersona(form?: NgForm) {
		console.log(form.value);
		console.log(this.model);
		if (this.model._id) {
			this.estadoCivilService.putEstadoCivil(this.model)
				.subscribe(res => {
					this.resetForm(form);
					this.getEstadoCivilList();
				});
			this.model._id = '';
		} else {
			this.estadoCivilService.postEstadoCivil(form.value)
				.subscribe(res => {
					this.getEstadoCivilList();
					this.resetForm(form);

				});
		}
	}

	resetForm(form?: NgForm) {
		if (form) {
			form.reset();
			this.estadoCivilService.selectedEstado = new EstadoCivil();
		}
	}

	deleteEstadoCivil(_id: string, form: NgForm) {
		if (confirm('estas seguro de eliminar el estado civil?')) {
			this.estadoCivilService.deleteEstadoCivil(_id)
				.subscribe(res => {
					this.getEstadoCivilList();
					this.resetForm(form);
				});
		}
	}
}
