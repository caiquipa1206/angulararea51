import { Component, OnInit } from '@angular/core';
import { TipoDocumentoService } from '../services/tipodocumento.service';
import { TipoDocumento } from '../models/tipoDocumento';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tipodocumento',
  templateUrl: './tipodocumento.component.html',
  styleUrls: ['./tipodocumento.component.css']
})
export class TipodocumentoComponent implements OnInit {

  constructor(private tipodocumentoService: TipoDocumentoService) { 
    this.getTipoPersonasList();
  }


  listaTipoPersona: any;
	idTipoPersona: string;
  ngOnInit() {
  }
  getTipoPersonasList() {
		this.tipodocumentoService.getTipoDocumentos().subscribe(data => {
			this.listaTipoPersona = data;
		});
  }
  model = this.tipodocumentoService.selectedTipodocumento;
  newtipopersona() {
		this.model = new TipoDocumento('',0, '');
  }
  edittipopersona(persona: TipoDocumento) {
		this.tipodocumentoService.getTipoDocumento(persona._id).subscribe(data => {
			console.log(data);
      this.tipodocumentoService.selectedTipodocumento = data as TipoDocumento;
     // console.log(this.tipoPersonaService.selectedTipoPersona );
      this.model = data as TipoDocumento;
      //console.log(this.model);
		});

  }
  addTipopersona(form?: NgForm) {
    console.log(form.value);
    console.log(this.model);
		 if (this.model._id) {
		 	this.tipodocumentoService.putTipoDocumento(this.model)
		 	.subscribe(res => {
		 			this.resetForm(form);
		 			this.getTipoPersonasList();
         });
         this.model._id = '';
		 } else {
		 	this.tipodocumentoService.postTipoDocumento(form.value)
		 		.subscribe(res => {
		 			this.getTipoPersonasList();
		 			this.resetForm(form);
		 			
		 		});
		 }
  }
  
  resetForm(form?: NgForm) {
		if (form) {
			form.reset();
			this.tipodocumentoService.selectedTipodocumento = new TipoDocumento();
		}
  }
  
  deleteTipoPersona(_id: string, form: NgForm) {
		if (confirm('estas seguro de eliminar el tipo documento?')) {
			this.tipodocumentoService.deleteTipoDocumento(_id)
				.subscribe(res => {
					this.getTipoPersonasList();
					this.resetForm(form);					
				});
		}
	}
}
