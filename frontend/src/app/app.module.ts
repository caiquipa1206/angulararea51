import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Route } from "@angular/router"
// Components
import { AppComponent } from './app.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { PersonaComponent } from './persona/persona.component';
import { TipopersonaComponent } from './tipopersona/tipopersona.component';
import { TipodocumentoComponent } from './tipodocumento/tipodocumento.component';
import { EstadocivilComponent } from './estadocivil/estadocivil.component';
import { LoginComponent } from './login/login.component';
import { UsuariologinComponent } from './usuariologin/usuariologin.component';
import { UsuariorgistroComponent } from './usuariorgistro/usuariorgistro.component';
import { MenuComponent } from './menu/menu.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthInterceptor } from './guards/AuthInterceptor';
//import { ReactiveFormsModule } from '@angular/forms';
const rutas: Route[] = [

	{ path: "", component: LoginComponent },
	{ path: "persona", component: PersonaComponent, canActivate: [AuthGuard] },
	{ path: "tipopersona", component: TipopersonaComponent, canActivate: [AuthGuard] },
	{ path: "tipodocumento", component: TipodocumentoComponent, canActivate: [AuthGuard] },
	{ path: "estadocivil", component: EstadocivilComponent, canActivate: [AuthGuard] },
	{ path: "registro", component: UsuariorgistroComponent }
]

@NgModule({
	declarations: [
		AppComponent,
		EmployeeComponent,
		PersonaComponent,
		TipopersonaComponent,
		TipodocumentoComponent,
		EstadocivilComponent,
		LoginComponent,
		UsuariologinComponent,
		UsuariorgistroComponent,
		MenuComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		//ReactiveFormsModule, ,
		HttpClientModule,
		RouterModule.forRoot(rutas),
	],
	providers: [
		AuthGuard,
		{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
