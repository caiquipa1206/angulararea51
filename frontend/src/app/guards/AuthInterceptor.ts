import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { ResponseLogin } from '../models/ResponseLogin';

//import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		if (currentUser) {
			req = req.clone({
				setHeaders: {
					'Content-Type': 'application/json; charset=utf-8',
					'Accept': 'application/json',
					'Authorization': (currentUser as ResponseLogin).accessToken//`Bearer ${AuthService.getToken()}`,
				},
			});
		}
		else {
			req = req.clone({
				setHeaders: {
					'Content-Type': 'application/json; charset=utf-8',
					'Accept': 'application/json'
				},
			});
		}

		//console.log(req)
		return next.handle(req);
	}
}