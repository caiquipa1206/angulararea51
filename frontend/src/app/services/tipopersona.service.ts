import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TipoPersona } from '../models/tipoPersona';
import { Persona } from '../models/persona';

@Injectable({
	providedIn: 'root'
})
export class TipoPersonaService {

	selectedTipoPersona: TipoPersona;
	tipopersonas: TipoPersona[];

	readonly URL_API = 'http://localhost:3000/api/tipopersonas';

	constructor(private http: HttpClient) {
		this.selectedTipoPersona = new TipoPersona();
	}

	postTipoPersona(tipopersona: TipoPersona) {
		return this.http.post(this.URL_API, tipopersona);
	}

	getTipoPersonas() {
		return this.http.get(this.URL_API);
	}

	getTipoPersona(_id: string) {
		return this.http.get(this.URL_API + `/${_id}`);
	}

	putTipoPersona(tipopersona: TipoPersona) {
		return this.http.put(this.URL_API + `/${tipopersona._id}`, tipopersona);
	}

	deleteTipoPersona(_id: string) {
		return this.http.delete(this.URL_API + `/${_id}`);
	}

	submitted = false;

	onSubmit() { this.submitted = true; }
}
