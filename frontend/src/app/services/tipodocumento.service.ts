import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TipoDocumento } from '../models/tipoDocumento';

@Injectable({
	providedIn: 'root'
})
export class TipoDocumentoService {

	selectedTipodocumento: TipoDocumento;
	tipodocumentos: TipoDocumento[];

	readonly URL_API = 'http://localhost:3000/api/tipodocumentos';

	constructor(private http: HttpClient) {
		this.selectedTipodocumento = new TipoDocumento();
	}

	postTipoDocumento(tipopersona: TipoDocumento) {
		return this.http.post(this.URL_API, tipopersona);
	}

	getTipoDocumentos() {
		return this.http.get(this.URL_API);
	}
	getTipoDocumento(_id: string) {
		return this.http.get(this.URL_API + `/${_id}`);
	}
	

	putTipoDocumento(tipodcumento: TipoDocumento) {
		return this.http.put(this.URL_API + `/${tipodcumento._id}`, tipodcumento);
	}

	deleteTipoDocumento(_id: string) {
		return this.http.delete(this.URL_API + `/${_id}`);
	}

}
