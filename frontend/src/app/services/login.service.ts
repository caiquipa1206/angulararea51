import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../models/usuario';

@Injectable({
	providedIn: 'root'
})
export class LoginService {

	selectedUsuario: Usuario;
	usuarios: Usuario[];

	readonly URL_API = 'http://localhost:3000/api/login';

	constructor(private http: HttpClient) {
		this.selectedUsuario = new Usuario();
		//console.log(this.selectedUsuario)
	}


	postLogin(usuario: Usuario) {
		console.log(usuario)
		return this.http.post(this.URL_API, usuario);
	}
	submitted = false;

	onSubmit() { this.submitted = true; }
}
