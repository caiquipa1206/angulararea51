import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { EstadoCivil } from '../models/estadocivil';

@Injectable({
	providedIn: 'root'
})
export class EstadoCivilService {

	selectedEstado: EstadoCivil;
	estadocivil: EstadoCivil[];

	readonly URL_API = 'http://localhost:3000/api/estadocivil';

	constructor(private http: HttpClient) {
		this.selectedEstado = new EstadoCivil();
	}

	postEstadoCivil(estadocivil: EstadoCivil) {
		return this.http.post(this.URL_API, estadocivil);
	}

	getEstadosCiviles() {
		return this.http.get(this.URL_API);
	}
	getEstadoCivil(_id: string) {
		//return this.http.get(this.URL_API + `/${_id}`);
		return this.http.get(this.URL_API + `/${_id}`);
	}
	putEstadoCivil(tipopersona: EstadoCivil) {
		return this.http.put(this.URL_API + `/${tipopersona._id}`, tipopersona);
	}

	deleteEstadoCivil(_id: string) {
		return this.http.delete(this.URL_API + `/${_id}`);
	}
}
