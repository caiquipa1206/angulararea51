import { Component, OnInit } from '@angular/core';
import { TipoPersonaService } from '../services/tipopersona.service';
import { TipoPersona } from '../models/tipoPersona';
import { TipoDocumento } from '../models/tipoDocumento';
import { TipoDocumentoService } from '../services/tipodocumento.service';
import { EstadoCivilService } from '../services/estadocivil.service';
import { PersonaService } from '../services/persona.service';
import { Persona } from '../models/persona';
import { NgForm } from '@angular/forms';
import { EstadoCivil } from '../models/estadocivil';
import { TipodocumentoComponent } from '../tipodocumento/tipodocumento.component';
declare var M: any;
@Component({
	selector: 'app-persona',
	templateUrl: './persona.component.html',
	styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

	listaTipoPersona: any;
	idTipoPersona: string;
	listaTipoDocumento: any;
	idTipoDocumento: string;
	listEstadoCivil: any;
	idEstadoCivil: string;
	listPersona: any;
	//data: any;
	//nameId: string;
	constructor(private tipoPersonaService: TipoPersonaService, private tipoDocumentoService: TipoDocumentoService, private estadoCivilService: EstadoCivilService, private personaService: PersonaService) {
		this.getTipoPersonasList();
		this.getTipoDocumentoList();
		this.getEstadoCivilList();
		this.getPersonasList();
	}

	ngOnInit() {
		//this.tipoPersona = this.tipoPersonaService.getTipoPersonas();
	}
	getTipoPersonasList() {
		this.tipoPersonaService.getTipoPersonas().subscribe(data => {
			this.listaTipoPersona = data;
		});
	}
	selectTipoPersona() {
		//alert(this.model.idTipoPersona);
	}
	getTipoDocumentoList() {
		this.tipoDocumentoService.getTipoDocumentos().subscribe(data => {
			this.listaTipoDocumento = data;
		});
	}
	selectTipoDocumento() {
		//alert(this.model.idTipoDocumento);
	}
	getEstadoCivilList() {
		this.estadoCivilService.getEstadosCiviles().subscribe(data => {
			this.listEstadoCivil = data;
		});
	}
	selectEstadoCivil() {
		//alert(this.model.estadoCivil);
	}
	editEmployee(persona: Persona) {
		// this.personaService.selectedPersona = persona;
		//console.log(persona);
		this.personaService.getPersona(persona._id).subscribe(data => {
			console.log(data);
			this.personaService.selectedPersona = data as Persona;
			this.model = data as Persona;
		});

	}

	addEmployee(form?: NgForm) {
		console.log(form.value);
		console.log(this.model);
		if (form.value._id) {
			this.personaService.putPersona(form.value)
				.subscribe(res => {
					this.resetForm(form);
					this.getPersonasList();
					//	M.toast({ html: 'Updated Successfully' });
				});
		} else {
			this.personaService.postPersona(form.value)
				.subscribe(res => {
					this.getTipoPersonas();
					this.resetForm(form);
					//M.toast({ html: 'Save successfully' });
				});
		}
	}
	deleteEmployee(_id: string, form: NgForm) {
		if (confirm('estas seguro de eliminar la persona?')) {
			this.personaService.deletePersona(_id)
				.subscribe(res => {
					this.getPersonasList();
					this.resetForm(form);
					//M.toast({ html: 'Deleted Succesfully' });
				});
		}
	}
	resetForm(form?: NgForm) {
		if (form) {
			form.reset();
			this.personaService.selectedPersona = new Persona();
		}
	}
	getPersonasList() {
		this.personaService.getPersonas().subscribe(data => {
			this.listPersona = data;
			for (let i = 0; i < this.listPersona.length; i++) {
				// this.tipoPersonaService.
				// 	console.log(this.listPersona[i].tipo); //use i instead of 0
				this.tipoPersonaService.getTipoPersona(this.listPersona[i].idTipoPersona).subscribe(data1 => {
					this.listPersona[i].idTipoPersona = (data1 as TipoPersona).tipo; //this.listPersona = data;
					console.log(data1);
				});
				this.tipoDocumentoService.getTipoDocumento(this.listPersona[i].idTipoDocumento).subscribe(data2 => {
					this.listPersona[i].idTipoDocumento = (data2 as TipoDocumento).tipoDocumento; //this.listPersona = data;
					console.log(data2);
				});
				this.estadoCivilService.getEstadoCivil(this.listPersona[i].estadoCivil).subscribe(data3 => {
					this.listPersona[i].estadoCivil = (data3 as EstadoCivil).estadoCivil; //this.listPersona = data;
					console.log(data3);
				});
			}
		});

	}


	/* getHeroes(): void {
		this.tipoPersonaService.getTipoPersonas().subscribe(heroes => this.heroes = heroes);
	} */


	//tipoDocumento = ['DNI', 'RUC', 'Pasaporte', 'Carnet de Extranjeria']

	//estadoCivil = ['Casado', 'Divorciado', 'Soltero', 'Viudo']

	model = this.personaService.selectedPersona;

	submitted = false;

	onSubmit() { this.submitted = true; }

	newHero() {
		this.model = new Persona('', '', '', '', '', '', '', '', '', '');
	}

	getTipoPersonas() {
		this.tipoPersonaService.getTipoPersonas()
			.subscribe(res => {
				this.tipoPersonaService.tipopersonas = res as TipoPersona[];
			});
	}

}

