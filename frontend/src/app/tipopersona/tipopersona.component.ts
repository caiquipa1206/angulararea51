import { Component, OnInit } from '@angular/core';
import { TipoPersonaService } from '../services/tipopersona.service';
import { TipoPersona } from '../models/tipoPersona';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-tipopersona',
  templateUrl: './tipopersona.component.html',
  styleUrls: ['./tipopersona.component.css']
})
export class TipopersonaComponent implements OnInit {

  constructor(private tipoPersonaService: TipoPersonaService) { 
    this.getTipoPersonasList();
  }
  listaTipoPersona: any;
	idTipoPersona: string;
  ngOnInit() {
  }
  getTipoPersonasList() {
		this.tipoPersonaService.getTipoPersonas().subscribe(data => {
			this.listaTipoPersona = data;
		});
  }
  model = this.tipoPersonaService.selectedTipoPersona;
  newtipopersona() {
		this.model = new TipoPersona('',0, '');
  }
  edittipopersona(persona: TipoPersona) {
		this.tipoPersonaService.getTipoPersona(persona._id).subscribe(data => {
			console.log(data);
      this.tipoPersonaService.selectedTipoPersona = data as TipoPersona;
     // console.log(this.tipoPersonaService.selectedTipoPersona );
      this.model = data as TipoPersona;
      //console.log(this.model);
		});

  }
  addTipopersona(form?: NgForm) {
    console.log(form.value);
    console.log(this.model);
		 if (this.model._id) {
		 	this.tipoPersonaService.putTipoPersona(this.model)
		 	.subscribe(res => {
		 			this.resetForm(form);
		 			this.getTipoPersonasList();
         });
         this.model._id = '';
		 } else {
		 	this.tipoPersonaService.postTipoPersona(form.value)
		 		.subscribe(res => {
		 			this.getTipoPersonasList();
		 			this.resetForm(form);
		 			
		 		});
		 }
  }
  
  resetForm(form?: NgForm) {
		if (form) {
			form.reset();
			this.tipoPersonaService.selectedTipoPersona = new TipoPersona();
		}
  }
  
  deleteTipoPersona(_id: string, form: NgForm) {
		if (confirm('estas seguro de eliminar el tipo persona?')) {
			this.tipoPersonaService.deleteTipoPersona(_id)
				.subscribe(res => {
					this.getTipoPersonasList();
					this.resetForm(form);
					//M.toast({ html: 'Deleted Succesfully' });
				});
		}
	}
}
