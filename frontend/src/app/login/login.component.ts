
import { ActivatedRoute, Router } from '@angular/router';
//import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Usuario } from '../models/usuario';

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ResponseLogin } from '../models/ResponseLogin';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	loading = false;
	submitted = false;
	returnUrl: string;
	valusuario: Usuario
	loginUrl: string
	//valorUsuario: Usuario
	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private loginService: LoginService
	) { }

	ngOnInit() {
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/persona';
		this.loginUrl = this.route.snapshot.queryParams['returnUrl'];
		localStorage.removeItem('currentUser');
	}
	//get f() { return this.loginForm.controls; }
	model = this.loginService.selectedUsuario;
	onSubmit(form?: NgForm) {

		console.log(form.value)
		// stop here if form is invalid
		if (form.invalid) {
			return;
		}


		//this.submitted = true;
		this.loginService.postLogin(form.value).subscribe(data => {

			const valorRetorno = data as ResponseLogin
			if (valorRetorno) {
				console.log(valorRetorno)
				if (valorRetorno.status == 200) {
					localStorage.setItem('currentUser', JSON.stringify(data));
					this.router.navigate([this.returnUrl]);
				}
				else {
					alert(valorRetorno.message)
					//this.resetForm(form);
					form.reset();
					//this.resetForm(form.value);
					//this.router.navigate([this.loginUrl]);
				}
			}
		});
		this.loading = true;
	}

	resetForm(form?: NgForm) {
		if (form) {
			form.reset();
			this.loginService.selectedUsuario = new Usuario();
		}
	}
}
