import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuariorgistroComponent } from './usuariorgistro.component';

describe('UsuariorgistroComponent', () => {
  let component: UsuariorgistroComponent;
  let fixture: ComponentFixture<UsuariorgistroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuariorgistroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuariorgistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
