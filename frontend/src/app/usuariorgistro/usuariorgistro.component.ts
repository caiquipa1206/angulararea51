import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { NgForm } from '@angular/forms';
import { Usuario } from '../models/usuario';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-usuariorgistro',
	templateUrl: './usuariorgistro.component.html',
	styleUrls: ['./usuariorgistro.component.css']
})
export class UsuariorgistroComponent implements OnInit {

	constructor(private usuarioService: UsuarioService, private router: Router, private route: ActivatedRoute, ) { }
	model = this.usuarioService.selectedUsuario;
	returnUrl: string;
	ngOnInit() {
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
	}
	addUsuario(form?: NgForm) {
		console.log(form.value);
		console.log(this.model);

		if (form.invalid) {
			return;
		}
		const valUsuario = new Usuario()
		valUsuario.usuario = (form.value as Usuario).usuario
		valUsuario.password = (form.value as Usuario).usuario
		valUsuario.nombre = (form.value as Usuario).nombre
		valUsuario.apellido = (form.value as Usuario).apellido
		valUsuario.refreshToken = "token"
		valUsuario.idperfil = 1
		valUsuario.identidad = 1
		this.usuarioService.postUsuario(valUsuario)
			.subscribe(res => {
				console.log(res)
				console.log(this.route.snapshot.queryParams['returnUrl'])
				//this.router.navigate(["http://localhost:4200/"]);
				alert((res as any).status)
				//this.router.navigate("http://localhost:4200");
				this.resetForm(form);

			});

	}
	resetForm(form?: NgForm) {
		if (form) {
			form.reset();
			this.usuarioService.selectedUsuario = new Usuario();
		}
	}
}
